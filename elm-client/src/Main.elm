module Main exposing (main)

import Browser
import Html exposing (Html)
import Http
import Url exposing (Url)


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Flags =
    ()


type alias Model =
    ()


init : Flags -> ( Model, Cmd Msg )
init _ =
    ( ()
    , sendEvent
    )


sendEvent =
    Http.request
        { method = "POST"
        , headers = []
        , url = "http://localhost:1234/"
        , body = Http.emptyBody
        , expect = Http.expectString GotResponse
        , timeout = Nothing
        , tracker = Nothing
        }


type Msg
    = GotResponse (Result Http.Error String)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotResponse result ->
            let
                _ =
                    Debug.log "Result" result
            in
            ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


view : Model -> Html Msg
view _ =
    Html.text "Here is Fana and Tad said why don't you search"
