var express = require('express')
var app = express()
var port = 1234;
var fs = require('fs');
var bodyParser = require('body-parser')
var cors = require('cors')
var path = require('path')

app.use(cors())
app.use(bodyParser.json())

let append_record_send_response = function (req, res) { 
  let event =  {  
    data: req.body ,
    time: new Date() 
  };
  let json = JSON.stringify(event, null, 2);
  fs.appendFile('server/events.json', json + "\n", handle_append_record_error);
  res.json({ok: true, response_message: 'This is the response message'});
}

let handle_append_record_error = function (err) {
  if (err) throw err;
}


app.post('/', append_record_send_response)
app.get('/', function (req, res) {
  res.sendFile('index.html', {root: path.join(__dirname, '../dist')});
})

app.listen(port, function () {
  console.log('server is ready on port :' + port + '\n');
});

